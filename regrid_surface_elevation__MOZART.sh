#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# Requires R.
# Driver for regrid.surface.elevation.r--configure as needed for your platform.
# Crops the following netCDF {file, datavar} pairs, saving output to separate file:
# * 2008N2O_restart.nc,N2O
# * 2008N2O_restart.nc,PS
# * ETOPO1_Ice_g_gmt4.grd.nc,zS: after renaming
# ** x->lon (to match 2008N2O_restart.nc)
# ** y->lat (to match 2008N2O_restart.nc)
# ** z->zS  (à la 2008N2O_restart.nc:PS)

# constants with some simple manipulations------------------------------

THIS_FN="$(basename $0)"
# will do the real work
export CALL_R_FN="$( echo -e ${THIS_FN} | sed -e 's/sh$/r/' )"

# modules on EMVL. TODO: fixme! (these fail from here, are currently just reminders.)
# You may not need them at all!
module add netcdf-4.1.2
#module add ncl_ncarg-6.0.0
#module add nco-4.0.5

# for R on EMVL: don't ask :-(
R_DIR='/usr/local/apps/R-2.15.2/intel-13.0/bin'
R="${R_DIR}/R"
RSCRIPT="${R_DIR}/Rscript"

# workspace
export WORK_DIR="$(pwd)" # keep it simple for now: same dir as top of repo

# will do the real work (restricting and plotting)
export CALL_R_FP="${WORK_DIR}/${CALL_R_FN}"

# for data display
export SIGDIGS='4' # significant digits

# for plotting
PDF_VIEWER='xpdf'  # whatever works on your platform
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"

# global lon-lat netCDFs: get from my repository, or download original if available

# template file (with the grid which we want the output to match)
TEMPLATE_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart_region_PS.nc'
TEMPLATE_FN="$(basename ${TEMPLATE_URI})"
# get both parts of the filename (before and after '.')
TEMPLATE_FN_ROOT="${TEMPLATE_FN%.*}"
TEMPLATE_FN_EXT="${TEMPLATE_FN##*.}"
export TEMPLATE_FP="${WORK_DIR}/${TEMPLATE_FN}"
export TEMPLATE_DATAVAR_NAME='PS'

# input: surface elevation from ETOPO1 cropped with restrict_lon_lat_to_region.sh
IN_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/ETOPO1_Ice_g_gmt4.grd_region_zS.nc'
IN_FN="$(basename ${IN_URI})"
IN_FN_ROOT="${IN_FN%.*}"
IN_FN_EXT="${IN_FN##*.}"
export IN_FP="${WORK_DIR}/${IN_FN}"
export IN_DATAVAR_NAME='zS'
# TODO: get from file, e.g., with NCL
export IN_DATAVAR_NA='-2147483648'
export IN_DATAVAR_BAND='NA'
IN_DATAVAR_UNIT='m'
IN_DATAVAR_LONGNAME='surface elevation'
IN_DATAVAR_COORD_X_NAME='lon'
IN_DATAVAR_COORD_Y_NAME='lat'

export RASTER_BRICK='FALSE'
export RASTER_ROTATE='FALSE'
export RASTER_PLOT='TRUE'  # can't make R use this???

# output: cropped surface elevation from ETOPO1 regridded to match template
OUT_FN_ROOT="${IN_FN_ROOT}_regrid"
OUT_FN="${OUT_FN_ROOT}.${IN_FN_EXT}"
export OUT_FP="${WORK_DIR}/${OUT_FN}"
export OUT_DATAVAR_NAME="${IN_DATAVAR_NAME}"
# note type=int in original, but probably should be float for regridding
# raster::writeRaster seems to autocast down unless instructed otherwise?
export OUT_DATAVAR_TYPE='FLT4S' # == float: see raster.pdf::dataType
export OUT_DATAVAR_NA="${IN_DATAVAR_NA}"
export OUT_DATAVAR_BAND="${IN_DATAVAR_BAND}"
export OUT_DATAVAR_UNIT="${IN_DATAVAR_UNIT}"
export OUT_DATAVAR_LONGNAME="${IN_DATAVAR_LONGNAME}"
export OUT_DATAVAR_COORD_X_NAME="${IN_DATAVAR_COORD_X_NAME}"
export OUT_DATAVAR_COORD_Y_NAME="${IN_DATAVAR_COORD_Y_NAME}"
# export OUT_DATAVAR_COORD_Z_NAME="${IN_DATAVAR_COORD_Z_NAME}"
# export OUT_DATAVAR_COORD_Z_UNIT="${IN_DATAVAR_COORD_Z_UNIT}"

OUT_PDF_FN="${OUT_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
export OUT_PDF_FP="${PDF_DIR}/${OUT_PDF_FN}" # path to PDF output
export OUT_PDF_HEIGHT='15'
export OUT_PDF_WIDTH='20'

# called scripts. TODO: R-package my code!

# this script does simple statistics, and is copied from ioapi-hack-R (also on github)
# TODO: R-package my code
STAT_SCRIPT_URI='https://github.com/TomRoche/GEIA_to_netCDF/raw/master/netCDF.stats.to.stdout.r'
STAT_SCRIPT_FN="$(basename ${STAT_SCRIPT_URI})"
export STAT_SCRIPT_FP="${WORK_DIR}/${STAT_SCRIPT_FN}"

# payload---------------------------------------------------------------

# setup-----------------------------------------------------------------

mkdir -p ${WORK_DIR}

# get scripts-----------------------------------------------------------

if [[ ! -r "${STAT_SCRIPT_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${STAT_SCRIPT_FP} ${STAT_SCRIPT_URI}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# get data------------------------------------------------------------

# template file
if [[ ! -r "${TEMPLATE_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${TEMPLATE_FP} ${TEMPLATE_URI}" \
    "ncdump -h ${TEMPLATE_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# elevation source
if [[ ! -r "${IN_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${IN_FP} ${IN_URI}" \
    "ncdump -h ${IN_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# call R script (the real work)---------------------------------------

# TODO: pass commandline args to Rscript
# punt: just use envvars :-(
# for CMD in \
#   "${RSCRIPT} ${CALL_R_FP}" \
${RSCRIPT} ${CALL_R_FP}
#   ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# After exiting R, show output files and display output PDF.
for CMD in \
  "ls -alht ${WORK_DIR} | head" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

if [[ -r "${OUT_PDF_FP}" ]] ; then
  for CMD in \
    "${PDF_VIEWER} ${OUT_PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else
  echo -e "ERROR: ${THIS_FN}: PDF output file='${OUT_PDF_FP}' not readable"
fi
