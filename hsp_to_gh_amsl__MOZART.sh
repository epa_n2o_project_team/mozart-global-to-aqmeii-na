#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# Requires NCL.
# Driver for hsp_to_gh_amsl.ncl--configure as needed for your platform.
# Convert MOZART hybrid sigma-pressure grid (dimensions=(hsp, lat, lon))
# to a more "Cartesian" grid with z=geometric height above mean sea level
# (dimensions=(gh_AMSL, lat, lon)). Should work with older NCL versions.

# constants with some simple manipulations------------------------------

THIS_FN="$(basename $0)"
# will do the real work

# TODO: create from THIS_FN, e.g., use `sed -e s/_/./g`
export CALL_NCL_FN='hsp_to_gh_amsl.ncl'

# modules on EMVL. TODO: fixme! (these fail from here, are currently just reminders.)
# You may not need them at all!
module add netcdf-4.1.2
#module add ncl_ncarg-6.0.0
# Temporarily using version on terrae installed by Jerry Herwehe (thanks!)
#module add nco-4.0.5

# Ensure $NCARG_ROOT is set for NCL--it should be in your dotfiles, but, JIC
# tlrPanP5
#NCL_VERSION='ncl_ncarg-6.1.0.Linux_Debian_x86_64_nodap_gcc445'
#export NCARG_ROOT="${HOME}/bin/${NCL_VERSION}"
## Only change default if absolutely necessary!
#NCL_EXEC='ncl'
# terrae temporary (they gotta upgrade RHEL)
NCL_VERSION='ncl_ncarg/ncl_ncarg-6.1.0.Linux_RedHat_x86_64_gcc412'
export NCARG_ROOT="/home/hhg/${NCL_VERSION}"
NCL_EXEC="${NCARG_ROOT}/bin/ncl"

# workspace
export WORK_DIR="$(pwd)" # keep it simple for now: same dir as top of repo
# will do the real work (restricting and plotting)
export CALL_NCL_FP="${WORK_DIR}/${CALL_NCL_FN}"

# # for plotting
# PDF_VIEWER='evince'  # whatever works on your platform
# # temporally disaggregate multiple plots
# DATE_FORMAT='%Y%m%d_%H%M'
# export PDF_DIR="${WORK_DIR}"

# source grids

# directories
SG_DIR="${WORK_DIR}" # keeping it simple
TG_DIR="${WORK_DIR}"

# files and URIs (from bitbucket repo)
SG_ORIGINAL_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart.nc'                       # uncropped
SG_ORIGINAL_FN="$(basename ${SG_ORIGINAL_URI})"
SG_ORIGINAL_FN_ROOT="${SG_ORIGINAL_FN%.*}"
SG_ORIGINAL_FN_EXT="${SG_ORIGINAL_FN##*.}"
NETCDF_EXT="${SG_ORIGINAL_FN_EXT}"                 # stay consistent
ASCII_EXT='txt'

SG_CROPPED_FN_ROOT="${SG_ORIGINAL_FN_ROOT}_region" # cropped
SG_CROPPED_FN="${SG_CROPPED_FN_ROOT}.${NETCDF_EXT}"

# MOZART-derived file with source datavar=N2O
SG_N2O_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart_region_N2O.nc'
SG_N2O_FN="$(basename ${SG_N2O_URI})"
export SG_N2O_FP="${SG_DIR}/${SG_N2O_FN}"

# MOZART-derived file with source datavar=PS
SG_PS_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart_region_PS.nc'
SG_PS_FN="$(basename ${SG_PS_URI})"
export SG_PS_FP="${SG_DIR}/${SG_PS_FN}"

# ETOPO1-derived file of surface elevations (only)
SG_ETOPO1_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/ETOPO1_Ice_g_gmt4.grd_region_zS.nc'
SG_ETOPO1_FN="$(basename ${SG_ETOPO1_URI})"
export SG_ETOPO1_FP="${SG_DIR}/${SG_ETOPO1_FN}"

# MOZART-derived file with all other datavars
SG_OTHER_URI="${SG_ORIGINAL_URI}"
export SG_OTHER_FN="$(basename ${SG_OTHER_URI})" # for doc strings
export SG_OTHER_FP="${SG_DIR}/${SG_OTHER_FN}"

# target grids

# datavar for elevations of layer interfaces, but horizontally geographical
export DATAVAR_Z_INT_NAME='z_int_geo'
# datavar for elevations of layer midpoints, but [ditto]
export DATAVAR_Z_MID_NAME='z_mid_geo'
# TODO: (RSN) create fully Cartesian, horizontally Cartesian datavars ...

# target netCDF file ~= ${SG_OTHER_FN} + new datavars above
TG_NETCDF_FN="${SG_CROPPED_FN_ROOT}_z.${NETCDF_EXT}"
export TG_NETCDF_FP="${TG_DIR}/${TG_NETCDF_FN}"

# target ASCII file: write ${DATAVAR_Z_INT_NAME} for data interchange with R
TG_ASCII_FN="${SG_CROPPED_FN_ROOT}_z.${ASCII_EXT}"
export TG_ASCII_FP="${TG_DIR}/${TG_ASCII_FN}"
# missing value for string output to be read by R
export TG_ASCII_NA='NA,NA,NA'
# not actually usable by @#$%^&! NCL::sprintf
# export TG_ASCII_LINE_FORMAT='%f,%f,%f' # sprintf string for CSV line
export TG_ASCII_DATUM_FORMAT='%f' # sprintf string for each datum in line

# payload---------------------------------------------------------------

# setup-----------------------------------------------------------------

mkdir -p ${WORK_DIR}

# get (or remove) data------------------------------------------------

# NCL will not overwrite target ASCII, so we will. (TODO: backup/rename)
if [[ -r "${TG_ASCII_FP}" ]] ; then
  echo -e "WARNING: ${THIS_FN}: about to delete previously output ASCII file='${TG_ASCII_FP}'"
  for CMD in \
    "rm ${TG_ASCII_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# NCL will not overwrite target netCDF, so we will. (TODO: backup/rename)
if [[ -r "${TG_NETCDF_FP}" ]] ; then
  echo -e "WARNING: ${THIS_FN}: about to delete previously output netCDF file='${TG_NETCDF_FP}'"
  for CMD in \
    "rm ${TG_NETCDF_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# MOZART-derived file with source datavar=N2O
SG_N2O_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart_region_N2O.nc'
SG_N2O_FN="$(basename ${SG_N2O_URI})"
export SG_N2O_FP="${SG_DIR}/${SG_N2O_FN}"
if [[ ! -r "${SG_N2O_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${SG_N2O_FP} ${SG_N2O_URI}" \
    "ncdump -h ${SG_N2O_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# MOZART-derived file with source datavar=PS
SG_PS_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart_region_PS.nc'
SG_PS_FN="$(basename ${SG_PS_URI})"
export SG_PS_FP="${SG_DIR}/${SG_PS_FN}"
if [[ ! -r "${SG_PS_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${SG_PS_FP} ${SG_PS_URI}" \
    "ncdump -h ${SG_PS_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# ETOPO1-derived file of surface elevations (only)
SG_ETOPO1_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/ETOPO1_Ice_g_gmt4.grd_region_zS.nc'
SG_ETOPO1_FN="$(basename ${SG_ETOPO1_URI})"
export SG_ETOPO1_FP="${SG_DIR}/${SG_ETOPO1_FN}"
if [[ ! -r "${SG_ETOPO1_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${SG_ETOPO1_FP} ${SG_ETOPO1_URI}" \
    "ncdump -h ${SG_ETOPO1_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# MOZART-derived file with all other datavars
SG_OTHER_URI="${SG_ORIGINAL_URI}"
SG_OTHER_FN="$(basename ${SG_OTHER_URI})"
export SG_OTHER_FP="${SG_DIR}/${SG_OTHER_FN}"
if [[ ! -r "${SG_OTHER_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${SG_OTHER_FP} ${SG_OTHER_URI}" \
    "ncdump -h ${SG_OTHER_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# call NCL script (the real work)---------------------------------------

# ${NCL_EXEC} # bail to NCL and copy script lines
# TODO: pass commandline args to NCL
# punt: just use envvars :-(
for CMD in \
  "${NCL_EXEC} ${CALL_NCL_FP}" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

# After exiting NCL, show output files ...

if [[ -r "${TG_ASCII_FP}" ||  -r "${TG_NETCDF_FP}" ]] ; then
  for CMD in \
    "ls -alht ${WORK_DIR} | head" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

if [[ -r "${TG_ASCII_FP}" ]] ; then
  for CMD in \
    "head ${TG_ASCII_FP}" \
    "tail ${TG_ASCII_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else # ![[ -r "${TG_ASCII_FP}" ]]
    echo -e "ERROR: ${THIS_FN}: output ASCII='${TG_ASCII_FP}' not readable"
fi

if [[ -r "${TG_NETCDF_FP}" ]] ; then
  for CMD in \
    "ncdump -h ${TG_NETCDF_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done

# # TODO: plot output
#   # ... and display output PDF.
#   if [[ -r "${OUT_PDF_FP}" ]] ; then
#     for CMD in \
#       "${PDF_VIEWER} ${OUT_PDF_FP} &" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   else
#     echo -e "ERROR: ${THIS_FN}: output PDF='${OUT_PDF_FP}' not readable"
#   fi

else # ![[ -r "${TG_NETCDF_FP}" ]]
    echo -e "ERROR: ${THIS_FN}: output netCDF='${TG_NETCDF_FP}' not readable"
fi
