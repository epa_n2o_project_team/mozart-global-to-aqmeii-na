#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# Driver for levelplot.netCDF.global.r--configure as needed for your platform.

# constants with some simple manipulations------------------------------

THIS_FN="$(basename $0)"
# will do the real work
# TODO: create from THIS_FN, e.g., use `sed -e s/_/./g`
export CALL_R_FN='levelplot.netCDF.global.r'

# modules on EMVL. TODO: fixme! (these fail from here, are currently just reminders.)
# You may not need them at all!
module add netcdf-4.1.2
module add nco-4.0.5

# for R on EMVL: don't ask :-(
R_DIR='/usr/local/apps/R-2.15.2/intel-13.0/bin'
R="${R_DIR}/R"
RSCRIPT="${R_DIR}/Rscript"

# workspace
export TEST_DIR="$(pwd)" # keep it simple for now: same dir as top of repo
mkdir -p ${TEST_DIR}

# will do the real work
export CALL_R_FP="${TEST_DIR}/${CALL_R_FN}"

# for plotting
PDF_VIEWER='xpdf'  # whatever works on your platform
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${TEST_DIR}"
PDF_FN="global_plot_$(date +${DATE_FORMAT}).pdf"
export PDF_FP="${TEST_DIR}/${PDF_FN}" # path to PDF output

# for data display
export SIGDIGS='4' # significant digits

# raw data from our collaborators
RAW_DATA_URI='https://github.com/downloads/TomRoche/MOZART_global_to_AQMEII-NA/2008N2O_restart.nc'
RAW_DATA_FN="$(basename ${RAW_DATA_URI})"
# get both parts of the filename (before and after '.')
RAW_DATA_FN_ROOT="${RAW_DATA_FN%.*}"
RAW_DATA_FN_EXT="${RAW_DATA_FN##*.}"
RAW_DATA_FP="${TEST_DIR}/${RAW_DATA_FN}"

export DATA_VAR_NAME='N2O'     # see ncdump output
export DATA_VAR_LONGNAME='N2O' # see ncdump output
export DATA_VAR_UNIT='ppmV'    # but only after pre-processing!
# export DATA_VAR_NA='-999.0'  # not currently defined!
export DATA_VAR_BAND='NA'    # input has no timestep defined: N2O(lev, lat, lon)

# compose input-data file name from raw-data file name
INPUT_DATA_FN="${RAW_PREFIX}_stripped.${RAW_SUFFIX}"
export INPUT_DATA_FP="${TEST_DIR}/${RAW_DATA_FN}"

# # compose output-data file name from input-data file name
# INPUT_DATA_FN_ROOT="${DATA_INPUT_FN%.*}"
# INPUT_DATA_FN_EXT="${DATA_INPUT_FN#*.}"
# OUTPUT_DATA_FN="${INPUT_DATA_FN_ROOT}_regrid.${INPUT_DATA_FN_EXT}"
# export OUTPUT_DATA_FP="${TEST_DIR}/${DATA_OUTPUT_FN}"

# this script drives image.plot, and is copied from ioapi-hack-R (also on github)
# TODO: R-package my code
PLOT_SCRIPT_URI='https://github.com/TomRoche/GEIA_to_netCDF/raw/master/plotLayersForTimestep.r'
PLOT_SCRIPT_FN="$(basename ${PLOT_SCRIPT_URI})"
export PLOT_SCRIPT_FP="${TEST_DIR}/${PLOT_SCRIPT_FN}"

# this script does simple statistics, and is copied from ioapi-hack-R (also on github)
# TODO: R-package my code
STAT_SCRIPT_URI='https://github.com/TomRoche/GEIA_to_netCDF/raw/master/netCDF.stats.to.stdout.r'
STAT_SCRIPT_FN="$(basename ${STAT_SCRIPT_URI})"
export STAT_SCRIPT_FP="${TEST_DIR}/${STAT_SCRIPT_FN}"

# payload-------------------------------------------------------------

# get data and scripts------------------------------------------------

# # Reuse stripped input data if it exists
# if [[ ! -r "${INPUT_DATA_FP}" ]] ; then
# # 1. Strip unneeded datavars and coordvars from "raw data" to create "input data" for ${CALL_R_FP}

  # note github has deprecated section=downloads: see https://github.com/blog/1302-goodbye-uploads
  # TODO: redo for bitbucket
  if [[ ! -r "${RAW_DATA_FP}" ]] ; then
    for CMD in \
      "wget --no-check-certificate -c -O ${RAW_DATA_FP} ${RAW_DATA_URI}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

#   # Now delete the unnecessary variables: TODO: encapsulate
#   for CMD in \
#     'ncks -x -v var1,var2 in.nc out.nc' \
#   ; do
#     echo -e "$ ${CMD}"
#     eval "${CMD}"
#   done

if [[ ! -r "${PLOT_SCRIPT_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${PLOT_SCRIPT_FP} ${PLOT_SCRIPT_URI}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

if [[ ! -r "${STAT_SCRIPT_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${STAT_SCRIPT_FP} ${STAT_SCRIPT_URI}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# call R script (the real work)---------------------------------------

echo # newline

# If this fails ...
echo -e "starting \`${RSCRIPT} ${CALL_R_FP}\`"
"${RSCRIPT}" "${CALL_R_FP}"

# ... just start R ...
# echo -e "starting \`${R}\`"
# "${R}"
# ... and source ${CALL_R_FP}, e.g.,
# > source('./GEIA.to.netCDF.r')

# start exit code copied from GEIA_to_netCDF.sh-----------------------
# TODO: refactor

# After exiting R, show cwd and display output PDF.
for CMD in \
  "ls -alht ${TEST_DIR}" \
  "${PDF_VIEWER} ${PDF_FP} &" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

#   end exit code copied from GEIA_to_netCDF.sh-----------------------
