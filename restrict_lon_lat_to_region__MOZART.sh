#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# Requires R.
# Driver for restrict.lon.lat.to.region.r--configure as needed for your platform.
# Crops the following netCDF {file, datavar} pairs, saving output to separate file:
# * 2008N2O_restart.nc,N2O
# * 2008N2O_restart.nc,PS
# * ETOPO1_Ice_g_gmt4.grd.nc,zS: after renaming
# ** x->lon (to match 2008N2O_restart.nc)
# ** y->lat (to match 2008N2O_restart.nc)
# ** z->zS  (à la 2008N2O_restart.nc:PS)

# constants with some simple manipulations------------------------------

THIS_FN="$(basename $0)"
# will do the real work
export CALL_R_FN="$( echo -e ${THIS_FN} | sed -e 's/sh$/r/' )"

# modules on EMVL. TODO: fixme! (these fail from here, are currently just reminders.)
# You may not need them at all!
module add netcdf-4.1.2
#module add ncl_ncarg-6.0.0
#module add nco-4.0.5

# for R on EMVL: don't ask :-(
R_DIR='/usr/local/apps/R-2.15.2/intel-13.0/bin'
R="${R_DIR}/R"
RSCRIPT="${R_DIR}/Rscript"

# workspace
export TEST_DIR="$(pwd)" # keep it simple for now: same dir as top of repo
mkdir -p ${TEST_DIR}

# will do the real work (restricting and plotting)
export CALL_R_FP="${TEST_DIR}/${CALL_R_FN}"

# for data display
export SIGDIGS='4' # significant digits

# for plotting
PDF_VIEWER='xpdf'  # whatever works on your platform
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${TEST_DIR}"

# global lon-lat netCDFs: get from my repository, or download original if available

# for N2O, PS
# TODO: download/process original
MOZART_URI='http://bitbucket.org/tlroche/mozart-global-to-aqmeii-na/downloads/2008N2O_restart.nc'
MOZART_FN="$(basename ${MOZART_URI})"
# get both parts of the filename (before and after '.')
MOZART_FN_ROOT="${MOZART_FN%.*}"
MOZART_FN_EXT="${MOZART_FN##*.}"
export MOZART_FP="${TEST_DIR}/${MOZART_FN}"

# for surface elevation
ETOPO1_GZ_URI='http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface/grid_registered/netcdf/ETOPO1_Ice_g_gmt4.grd.gz'
ETOPO1_FN=$(echo $(basename ${ETOPO1_GZ_URI%%\?*}) | sed -e 's/gz$/nc/') # ensure standard suffix
ETOPO1_FN_ROOT="${ETOPO1_FN%.*}"
ETOPO1_FN_EXT="${ETOPO1_FN##*.}"
export ETOPO1_FP="${TEST_DIR}/${ETOPO1_FN}"

# metadata for datavars and their attributes. TODO: read from netCDF

# the N2O concentrations: N2O(lev, lat, lon)
N2O_DATAVAR_NAME='N2O'
# note type==double in original
N2O_DATAVAR_TYPE='FLT8S' # == double: see raster.pdf::dataType
N2O_DATAVAR_UNIT='ppmV'  # but only after pre-processing!
N2O_DATAVAR_NA='-1.0'    # assigned here, not in netCDF
N2O_DATAVAR_BAND='NA'    # input has no timestep defined: N2O(lev, lat, lon)
N2O_REGIONAL_FN_ROOT="${MOZART_FN_ROOT}_region_${N2O_DATAVAR_NAME}" # used below
N2O_REGIONAL_FN="${N2O_REGIONAL_FN_ROOT}.${MOZART_FN_EXT}"
N2O_REGIONAL_FP="${TEST_DIR}/${N2O_REGIONAL_FN}"
N2O_PDF_FN="${N2O_REGIONAL_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
N2O_PDF_FP="${PDF_DIR}/${N2O_PDF_FN}" # path to PDF output
N2O_PDF_HEIGHT='5'
N2O_PDF_WIDTH='100'

# following datavars required for vertical coordinate conversion (later): hybrid sigma-pressure -> elevation

# double PS(lat, lon)
SURF_PRESSURE_DATAVAR_NAME='PS'
# note type==double in original
SURF_PRESSURE_DATAVAR_TYPE='FLT8S' # == double: see raster.pdf::dataType
SURF_PRESSURE_DATAVAR_UNIT='Pa'
SURF_PRESSURE_DATAVAR_LONGNAME='Surface Pressure' # original case
SURF_PRESSURE_DATAVAR_NA='-1.0' # assigned here, not in netCDF
SURF_PRESSURE_DATAVAR_BAND='NA' # input has no timestep defined
SURF_PRESSURE_DATAVAR_COORD_X_NAME='lon'
SURF_PRESSURE_DATAVAR_COORD_Y_NAME='lat'
SURF_PRESSURE_GLOBAL_FP="${MOZART_FP}"
SURF_PRESSURE_REGIONAL_FN_ROOT="${MOZART_FN_ROOT}_region_${SURF_PRESSURE_DATAVAR_NAME}" # used below
SURF_PRESSURE_REGIONAL_FN="${SURF_PRESSURE_REGIONAL_FN_ROOT}.${MOZART_FN_EXT}"
SURF_PRESSURE_REGIONAL_FP="${TEST_DIR}/${SURF_PRESSURE_REGIONAL_FN}"
SURF_PRESSURE_PDF_FN="${SURF_PRESSURE_REGIONAL_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
SURF_PRESSURE_PDF_FP="${PDF_DIR}/${SURF_PRESSURE_PDF_FN}" # path to PDF output

# int z(y, x) -> double zS(lat, lon)
SURF_ELEVATION_DATAVAR_NAME_IN='z'
SURF_ELEVATION_DATAVAR_NAME_OUT='zS'
# note type=int in original, but probably should be float for regridding
SURF_ELEVATION_DATAVAR_TYPE='FLT4S' # == float: see raster.pdf::dataType
SURF_ELEVATION_DATAVAR_UNIT='m'
SURF_ELEVATION_DATAVAR_LONGNAME='surface elevation' # assigned here
SURF_ELEVATION_DATAVAR_NA='-2147483648' # use value from original
SURF_ELEVATION_DATAVAR_BAND='NA' # input has no timestep defined
SURF_ELEVATION_DATAVAR_COORD_X_NAME_IN='x'
SURF_ELEVATION_DATAVAR_COORD_X_NAME_OUT='lon'
SURF_ELEVATION_DATAVAR_COORD_Y_NAME_IN='y'
SURF_ELEVATION_DATAVAR_COORD_Y_NAME_OUT='lat'
SURF_ELEVATION_GLOBAL_FP="${ETOPO1_FP}"
SURF_ELEVATION_REGIONAL_FN_ROOT="${ETOPO1_FN_ROOT}_region_${SURF_ELEVATION_DATAVAR_NAME_OUT}" # used below
SURF_ELEVATION_REGIONAL_FN="${SURF_ELEVATION_REGIONAL_FN_ROOT}.${ETOPO1_FN_EXT}"
SURF_ELEVATION_REGIONAL_FP="${TEST_DIR}/${SURF_ELEVATION_REGIONAL_FN}"
SURF_ELEVATION_PDF_FN="${SURF_ELEVATION_REGIONAL_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
SURF_ELEVATION_PDF_FP="${PDF_DIR}/${SURF_ELEVATION_PDF_FN}" # path to PDF output

# called scripts. TODO: R-package my code!

# # this script drives image.plot, and is copied from ioapi-hack-R (also on github)
# PLOT_SCRIPT_URI='https://github.com/TomRoche/GEIA_to_netCDF/raw/master/plotLayersForTimestep.r'
# PLOT_SCRIPT_FN="$(basename ${PLOT_SCRIPT_URI})"
# export PLOT_SCRIPT_FP="${TEST_DIR}/${PLOT_SCRIPT_FN}"

# this script does simple statistics, and is copied from ioapi-hack-R (also on github)
# TODO: R-package my code
STAT_SCRIPT_URI='https://github.com/TomRoche/GEIA_to_netCDF/raw/master/netCDF.stats.to.stdout.r'
STAT_SCRIPT_FN="$(basename ${STAT_SCRIPT_URI})"
export STAT_SCRIPT_FP="${TEST_DIR}/${STAT_SCRIPT_FN}"

# payload---------------------------------------------------------------

# get scripts-----------------------------------------------------------

# if [[ ! -r "${PLOT_SCRIPT_FP}" ]] ; then
#   for CMD in \
#     "wget --no-check-certificate -c -O ${PLOT_SCRIPT_FP} ${PLOT_SCRIPT_URI}" \
#   ; do
#     echo -e "$ ${CMD}"
#     eval "${CMD}"
#   done
# fi

if [[ ! -r "${STAT_SCRIPT_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${STAT_SCRIPT_FP} ${STAT_SCRIPT_URI}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# get data------------------------------------------------------------

# N2O, PS source
if [[ ! -r "${MOZART_FP}" ]] ; then
  for CMD in \
    "wget --no-check-certificate -c -O ${MOZART_FP} ${MOZART_URI}" \
    "ncdump -h ${MOZART_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# elevation source: is gzip'ed, must be unpacked first
if [[ ! -r "${ETOPO1_FP}" ]] ; then
  for CMD in \
    "curl -C - ${ETOPO1_GZ_URI} | gunzip -c > ${ETOPO1_FP}" \
    "ncdump -h ${ETOPO1_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

# call R script (the real work)---------------------------------------

echo # newline

# # for each of our 3 tuples={filepath_in, datavar_in, filepath_out, datavar_out} 
# for PAIR in \
#   "${N2O_GLOBAL_FP},${N2O_DATAVAR_NAME_IN},${N2O_REGIONAL_FP},${N2O_DATAVAR_NAME_OUT}" \
#   "${SURF_PRESSURE_GLOBAL_FP},${SURF_PRESSURE_DATAVAR_NAME_IN},${SURF_PRESSURE_REGIONAL_FP},${SURF_PRESSURE_DATAVAR_NAME_OUT}" \
#   "${SURF_ELEVATION_GLOBAL_FP},${SURF_ELEVATION_DATAVAR_NAME_IN},${SURF_ELEVATION_REGIONAL_FP},${SURF_ELEVATION_DATAVAR_NAME_OUT}" \
# ; do
#    "${RSCRIPT} ${CALL_R_FP} in.fp='' in.datavar.name='' out.fp='' out.datavar.name=''" \
# done

# TODO: make *1* R invocation with all that package loading!

# # Note R-script argument quoting is *very annoying*!
# # in.fp='${MOZART_FP}' \
# # in.fp=\"${MOZART_FP}\" \
# # in.fp=${MOZART_FP} \
# for CMD in \
#   "${RSCRIPT} ${CALL_R_FP} \
# 'in.fp=\"${MOZART_FP}\"' \
# # fails with
# # > Error in eval.with.vis(expr, envir, enclos) : unknown argument='in.fp'
# in.datavar.name=\"N2O\" \
# in.datavar.band=\"NA\" \
# out.fp=\"${N2O_REGIONAL_FP}\" \
# out.datavar.name=\"N2O\" \
# raster.brick=\"TRUE\" \
# raster.rotate=\"TRUE\" \
# raster.plot=\"TRUE\" \
# out.datavar.na=\"${N2O_DATAVAR_NA}\" \
# out.datavar.unit=\"${N2O_DATAVAR_UNIT}\" \
# out.datavar.longname=\"N2O concentration\" \
# out.datavar.coord.x.name=\"lon\" \
# out.datavar.coord.y.name=\"lat\" \
# out.datavar.coord.z.name=\"lev\" \
# out.datavar.coord.z.unit=\"hybrid_sigma_pressure\"" \
#   ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# punt: just use envvars :-(

# take 1: N2O
export IN_FP="${MOZART_FP}"
export IN_DATAVAR_NAME="${N2O_DATAVAR_NAME}"
export IN_DATAVAR_BAND='NA'
export IN_DATAVAR_TYPE="${N2O_DATAVAR_TYPE}"
export OUT_FP="${N2O_REGIONAL_FP}"
export OUT_DATAVAR_NAME='N2O'
export RASTER_BRICK='TRUE'
export RASTER_ROTATE='TRUE'
export RASTER_PLOT='TRUE'  # can't make R use this???
export OUT_DATAVAR_NA="${N2O_DATAVAR_NA}"
export OUT_DATAVAR_UNIT="${N2O_DATAVAR_UNIT}"
export OUT_DATAVAR_LONGNAME='N2O concentration'
export OUT_DATAVAR_COORD_X_NAME='lon'
export OUT_DATAVAR_COORD_Y_NAME='lat'
export OUT_DATAVAR_COORD_Z_NAME='lev'
export OUT_DATAVAR_COORD_Z_UNIT='hybrid_sigma_pressure'
export PDF_FP="${N2O_PDF_FP}" # path to PDF output
export PDF_HEIGHT='100'
export PDF_WIDTH='5'
# for CMD in \
#   "${RSCRIPT} ${CALL_R_FP}" \
${RSCRIPT} ${CALL_R_FP}
#   ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# After exiting R, show output files and display output PDF.
for CMD in \
  "ls -alht ${TEST_DIR}" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

if [[ -r "${PDF_FP}" ]] ; then
  for CMD in \
    "${PDF_VIEWER} ${PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else
  echo -e "ERROR: ${THIS_FN}: PDF output file='${PDF_FP}' not readable"
fi

# take 2: surface pressure
export IN_FP="${MOZART_FP}"
export IN_DATAVAR_NAME="${SURF_PRESSURE_DATAVAR_NAME}"
export IN_DATAVAR_BAND='NA'
export IN_DATAVAR_TYPE="${SURF_PRESSURE_DATAVAR_TYPE}"
export OUT_FP="${SURF_PRESSURE_REGIONAL_FP}"
export OUT_DATAVAR_NAME="${SURF_PRESSURE_DATAVAR_NAME}"
export RASTER_BRICK='FALSE' # only one layer
export RASTER_ROTATE='TRUE'
export RASTER_PLOT='TRUE'  # can't make R use this???
export OUT_DATAVAR_NA="${SURF_PRESSURE_DATAVAR_NA}"
export OUT_DATAVAR_UNIT="${SURF_PRESSURE_DATAVAR_UNIT}"
export OUT_DATAVAR_LONGNAME="${SURF_PRESSURE_DATAVAR_LONGNAME}"
export OUT_DATAVAR_COORD_X_NAME='lon'
export OUT_DATAVAR_COORD_Y_NAME='lat'
export OUT_DATAVAR_COORD_Z_NAME=''
export OUT_DATAVAR_COORD_Z_UNIT=''
export PDF_FP="${SURF_PRESSURE_PDF_FP}" # path to PDF output
export PDF_HEIGHT='15'
export PDF_WIDTH='20'
# for CMD in \
#   "${RSCRIPT} ${CALL_R_FP}" \
${RSCRIPT} ${CALL_R_FP}
#   ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# After exiting R, show output files and display output PDF.
for CMD in \
  "ls -alht ${TEST_DIR}" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

if [[ -r "${PDF_FP}" ]] ; then
  for CMD in \
    "${PDF_VIEWER} ${PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else
  echo -e "ERROR: ${THIS_FN}: PDF output file='${PDF_FP}' not readable"
fi

# take 3: surface elevation
export IN_FP="${ETOPO1_FP}"
export IN_DATAVAR_NAME="${SURF_ELEVATION_DATAVAR_NAME_IN}"
export IN_DATAVAR_BAND='NA'
export IN_DATAVAR_TYPE="${SURF_ELEVATION_DATAVAR_TYPE}"
export OUT_FP="${SURF_ELEVATION_REGIONAL_FP}"
export OUT_DATAVAR_NAME="${SURF_ELEVATION_DATAVAR_NAME_OUT}"
export RASTER_BRICK='FALSE' # only one layer
export RASTER_ROTATE='FALSE'
export RASTER_PLOT='TRUE'  # can't make R use this???
export OUT_DATAVAR_NA="${SURF_ELEVATION_DATAVAR_NA}"
export OUT_DATAVAR_UNIT="${SURF_ELEVATION_DATAVAR_UNIT}"
export OUT_DATAVAR_LONGNAME="${SURF_ELEVATION_DATAVAR_LONGNAME}"
export OUT_DATAVAR_COORD_X_NAME='lon'
export OUT_DATAVAR_COORD_Y_NAME='lat'
# export OUT_DATAVAR_COORD_Z_NAME=''
# export OUT_DATAVAR_COORD_Z_UNIT=''
export PDF_FP="${SURF_ELEVATION_PDF_FP}" # path to PDF output
export PDF_HEIGHT='15'
export PDF_WIDTH='20'
# for CMD in \
#   "${RSCRIPT} ${CALL_R_FP}" \
${RSCRIPT} ${CALL_R_FP}
#   ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# # int z(y, x) -> double zS(lat, lon)
# SURF_ELEVATION_DATAVAR_NAME_IN='z'
# SURF_ELEVATION_DATAVAR_NAME_OUT='zS'
# SURF_ELEVATION_DATAVAR_LONGNAME='surface elevation' # assigned here
# SURF_ELEVATION_DATAVAR_UNIT='m'
# # SURF_ELEVATION_DATAVAR_NA='-1.0' # use value from original
# SURF_ELEVATION_DATAVAR_BAND='NA' # input has no timestep defined
# SURF_ELEVATION_DATAVAR_COORD_X_NAME_IN='x'
# SURF_ELEVATION_DATAVAR_COORD_X_NAME_OUT='lon'
# SURF_ELEVATION_DATAVAR_COORD_Y_NAME_IN='y'
# SURF_ELEVATION_DATAVAR_COORD_Y_NAME_OUT='lat'
# # SURF_ELEVATION_GLOBAL_FP="${ETOPO1_FP}"
# SURF_ELEVATION_REGIONAL_FN_ROOT="${ETOPO1_FN_ROOT}_region_${SURF_ELEVATION_DATAVAR_NAME_OUT}" # used below
# SURF_ELEVATION_REGIONAL_FN="${SURF_ELEVATION_REGIONAL_FN_ROOT}.${ETOPO1_FN_EXT}"
# SURF_ELEVATION_REGIONAL_FP="${TEST_DIR}/${SURF_ELEVATION_REGIONAL_FN}"
# SURF_ELEVATION_PDF_FN="${SURF_ELEVATION_REGIONAL_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
# SURF_ELEVATION_PDF_FP="${PDF_DIR}/${SURF_ELEVATION_PDF_FN}" # path to PDF output

# After exiting R, show output files and display output PDF.
for CMD in \
  "ls -alht ${TEST_DIR} | head" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

if [[ -r "${PDF_FP}" ]] ; then
  for CMD in \
    "${PDF_VIEWER} ${PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else
  echo -e "ERROR: ${THIS_FN}: PDF output file='${PDF_FP}' not readable"
fi
