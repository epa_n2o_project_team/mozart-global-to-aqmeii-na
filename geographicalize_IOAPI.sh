#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# Requires R, NCL.
# Driver for [prune_IOAPI.ncl, geographicalize_IOAPI.r]--configure as needed for your platform.

# Convert netCDF IOAPI file with numerous datavars into one with a single (non-IOAPI) datavar
# and convert that datavar from dimensions=(TSTEP, LAY, ROW, COL) to (TSTEP, LAY, lat, lon)

# constants with some simple manipulations------------------------------

THIS_FN="$(basename $0)"
# helpers will do the real work
# THIS_PRE_NCL="${THIS_FN%.*}" # everything left of the extension
THIS_PRE_NCL='prune_IOAPI'
THIS_PRE_R="${THIS_FN%.*}" # everything left of the extension
export CALL_NCL_FN="${THIS_PRE_NCL}.ncl"
export CALL_R_FN="${THIS_PRE_R}.r"

# modules on EMVL. TODO: fixme! (these fail from here, are currently just reminders.)
# You may not need them at all!
module add netcdf-4.1.2
module add ncl_ncarg-6.0.0

# helper VMs

## NCL
### Ensure $NCARG_ROOT is set for NCL--it should be in your dotfiles, but, JIC
### tlrPanP5
# NCL_VERSION='ncl_ncarg-6.1.0.Linux_Debian_x86_64_nodap_gcc445'
# export NCARG_ROOT="${HOME}/bin/${NCL_VERSION}"
# NCL_EXEC='ncl'
### terrae
NCL_VERSION='ncl_ncarg/ncl_ncarg-6.1.0.Linux_RedHat_x86_64_gcc412'
# in Jerry Herwehe's homespace temporarily (EMVL has gotta upgrade RHEL)
export NCARG_ROOT="/home/hhg/${NCL_VERSION}"
NCL_EXEC="${NCARG_ROOT}/bin/ncl"

## R
### tlrPanP5
# R_EXEC='Rscript' # TODO: pass commandline args
### terrae
R_DIR='/usr/local/apps/R-2.15.2/intel-13.0/bin'
R="${R_DIR}/R"
RSCRIPT="${R_DIR}/Rscript"
R_EXEC="${RSCRIPT}"

# workspace
export WORK_DIR="$(pwd)" # keep it simple for now: same dir as top of repo
# helpers
export CALL_NCL_FP="${WORK_DIR}/${CALL_NCL_FN}"
export CALL_R_FP="${WORK_DIR}/${CALL_R_FN}"

# TODO: visualization!
# # for plotting
# ## tlrPanP5
# PDF_VIEWER='evince'
# ## terrae
# PDF_VIEWER='xpdf'
# # temporally disaggregate multiple plots
# DATE_FORMAT='%Y%m%d_%H%M'
# export PDF_DIR="${WORK_DIR}"

NETCDF_EXT='nc'  # stay consistent
ASCII_EXT='txt'

# source grids (SG)

## directories
SG_DIR="${WORK_DIR}" # keeping it simple

# the original, ultimate, ur-source of this data
# eventually get this from KNB?
# SG_URI='https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na/downloads/2008N2O_restart.nc'
# SG_FN="$(basename ${SG_URI})"
SG_FR='ICON_CB05AE5_US12_2007356'
SG_FN="${SG_FR}.${NETCDF_EXT}"
export SG_FP="${SG_DIR}/${SG_FN}"

## source datavar names
export SG_DATAVAR_GRID_NAME='NO2'    # any will do, just need one for grid getting
export SG_DATAVAR_IOAPI_NAME='TFLAG' # only this name will do: it's special to IOAPI
# export SG_DATAVAR_GRID_NAMES_VEC='ROW,COL,LAY,TSTEP,CONC'
export SG_DATAVAR_GRID_UNITS='m'     # meters: see R::M3::get.coord.for.dimension
# TODO: get SG_LEVELS_HSP_RAW from sg global attribute=VGLVLS
export SG_LEVELS_HSP_RAW='1.0,0.995,0.99,0.985,0.98,0.97,0.96,0.95,0.94,0.93,0.92,0.91,0.9,0.88,0.86,0.84,0.82,0.8,0.77,0.74,0.7,0.65,0.6,0.55,0.5'

# intermediate grids (IG)

## directories
IG_DIR="${WORK_DIR}"

## files
IG_FR="${SG_FR}_pruned"

### netCDF
IG_NETCDF_FN="${IG_FR}.${NETCDF_EXT}"
export IG_NETCDF_FP="${IG_DIR}/${IG_NETCDF_FN}"

### CSV of gridded datavar only, for use by R
IG_CSV_FN="${IG_FR}.${ASCII_EXT}"
export IG_CSV_FP="${IG_DIR}/${IG_CSV_FN}"
export IG_CSV_NA='-1.0' # to be converted to float
export IG_CSV_INTEG_F='%i' # `sprinti` format string for integer
export IG_CSV_FLOAT_F='%f' # `sprintf` format string for float

# target grids (TG)

## directories
TG_DIR="${WORK_DIR}"

## files
TG_FR="${IG_FR}_geoged" # 'geographicalized' is just too long

TG_CSV_FN="${TG_FR}.${ASCII_EXT}"
export TG_CSV_FP="${TG_DIR}/${TG_CSV_FN}"

TG_NETCDF_FN="${TG_FR}.${NETCDF_EXT}"
export TG_NETCDF_FP="${TG_NETCDF_DIR}/${TG_NETCDF_FN}" # for input to ESMF_RegridWeightGen

# ### global attributes
# # export TG_TITLE="${TG_DATAVAR_LONGNAME} derived from ${SG_ORIGINAL_FP}" # dependency below
# export TG_SOURCEFILE="${SG_ORIGINAL_URI}"
# export TG_CONVENTIONS='ESMF Unstructured Grid File Format'
# export TG_HISTORY='see https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na'
# # export TG_CREATIONDATE="$(date)" # set @ runtime

# ### dimensions
# export TG_DIM_ROW_NAME='nodeCount'  # from ESMF Unstructured Grid File Format
# # export TG_DIM_ROW_COUNT='' # will compute
# export TG_DIM_COL_NAME='coordDim'   # from ESMF Unstructured Grid File Format
# export TG_DIM_COL_COUNT="${SG_ASCII_COL_COUNT}"

# ### datavar for Cartesianized elevations, i.e., (x,y,z)
# export TG_DATAVAR_NAME='nodeCoords' # from ESMF Unstructured Grid File Format
# export TG_DATAVAR_LONGNAME='Cartesianized regional grid nodes'
# export TG_TITLE="${TG_DATAVAR_LONGNAME} derived from ${TG_SOURCEFILE}"
# export TG_DATAVAR_UNITS='m along dimension'

# payload---------------------------------------------------------------

## setup-----------------------------------------------------------------

mkdir -p ${WORK_DIR}

### get (or remove) data------------------------------------------------

#### get ASCII source from web if not local
if [[ ! -r "${SG_FP}" ]] ; then
#   for CMD in \
#     "wget --no-check-certificate -c -O ${SG_FP} ${IN_URI}" \
#     "head ${SG_FP}" \
#     "tail ${SG_FP}" \
#   ; do
#     echo -e "$ ${CMD}"
#     eval "${CMD}"
#   done
  echo -e "${THIS_FN}: ERROR: cannot find source grid='${SG_FP}'"
  exit 1
fi

## call NCL script-------------------------------------------------------

if [[ -r "${CALL_NCL_FP}" &&  -r "${SG_FP}" ]] ; then

  # NCL will not overwrite netCDF, so we will. (TODO: backup/rename)
  if [[ -r "${IG_NETCDF_FP}" ]] ; then
    echo -e "WARNING: ${THIS_FN}: about to delete previously output netCDF file='${IG_NETCDF_FP}'"
    for CMD in \
      "rm ${IG_NETCDF_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

  # TODO: pass args to NCL by commandline, instead of envvars
  # ${NCL_EXEC} # bail to NCL and copy script lines
  for CMD in \
    "${NCL_EXEC} ${CALL_NCL_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done

else
  echo -e "${THIS_FN}: ERROR: cannot find NCL pruning script='${CALL_NCL_FP}'"
  exit 2
fi

## call R script---------------------------------------------------------

# ${R_EXEC} # bail to R and copy script lines
# if [[ -r "${CALL_R_FP}" && -r "${IG_CSV_FP}" ]] ; then
if [[ -r "${CALL_R_FP}" && -r "${IG_NETCDF_FP}" ]] ; then

#  # R will not overwrite netCDF, so we will. (TODO: backup/rename)
#  if [[ -r "${TG_NETCDF_FP}" ]] ; then
#    echo -e "WARNING: ${THIS_FN}: about to delete previously output netCDF file='${TG_NETCDF_FP}'"
#    for CMD in \
#      "rm ${TG_NETCDF_FP}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi

 # R will not overwrite CSV, so we will. (TODO: backup/rename)
 if [[ -r "${TG_CSV_FP}" ]] ; then
   echo -e "WARNING: ${THIS_FN}: about to delete previously output CSV file='${TG_CSV_FP}'"
   for CMD in \
     "rm ${TG_CSV_FP}" \
   ; do
     echo -e "$ ${CMD}"
     eval "${CMD}"
   done
 fi

  # TODO: pass args to Rscript by commandline, instead of envvars
  for CMD in \
    "${R_EXEC} ${CALL_R_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done

else
  echo -e "${THIS_FN}: ERROR: cannot find R geographicalization script='${CALL_R_FP}'"
#  exit 3
fi

## show output files--------------------------------------------------

for CMD in \
  "ls -alht ${WORK_DIR}/*.{nc,txt} | head" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done
# $ ls -alht /home/tlroche/code/regridding/MOZART_global_to_AQMEII-NA | head
# ...

# if [[ -r "${TG_NETCDF_FP}" ]] ; then
#   for CMD in \
#     "ls -alh ${TG_NETCDF_FP}" \
#     "ncdump -h ${TG_NETCDF_FP}" \
#   ; do
#     echo -e "$ ${CMD}"
#     eval "${CMD}"
#   done
# else
#     echo -e "ERROR: ${THIS_FN}: output netCDF='${TG_NETCDF_FP}' not readable"
# fi

if [[ -r "${TG_CSV_FP}" ]] ; then
  for CMD in \
    "head ${TG_CSV_FP}" \
    "tail ${TG_CSV_FP}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
else
    echo -e "ERROR: ${THIS_FN}: output CSV='${TG_CSV_FP}' not readable"
fi

# # TODO: plot output
#   # ... and display output PDF.
#   if [[ -r "${OUT_PDF_FP}" ]] ; then
#     for CMD in \
#       "${PDF_VIEWER} ${OUT_PDF_FP} &" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   else
#     echo -e "ERROR: ${THIS_FN}: output PDF='${OUT_PDF_FP}' not readable"
#   fi
