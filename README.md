*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# description

## problem

I have a 3D global N2O inventory (i.e., atmospheric N2O concentrations for each cell in a 3D grid), which is the *input* to this project (which is a small part of a larger project involving [CMAQ][CMAQ main page @ CMAS]). The input is unprojected, with grid dimensions=(longitude, latitude, [hybrid sigma-pressure][hybrid sigma-pressure @ wikipedia]). I need to convert that input into a form (my *output*) that can be consumed by a particular CMAQ run. For that, my output must be

* regional (only over the [AQMEII-NA domain][EPA implementation of AQMEII-NA], rather than over the whole earth)
* projected to [Lambert conformal conic][LCC @ wikipedia] (LCC)
* also 3D, with grid dimensions=(col, row, [HσP][hybrid sigma-pressure @ wikipedia])

Note also that the input and output differ in both the vertical and horizontal extents: the layer heights, layer count, and top height all differ.

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[CMAQ main page @ CMAS]: http://www.cmaq-model.org/
[EPA implementation of AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[LCC @ wikipedia]: http://en.wikipedia.org/wiki/Lambert_conformal_conic
[hybrid sigma-pressure @ wikipedia]: http://en.wikipedia.org/wiki/Sigma_coordinate_system#hybrid_sigma-pressure

## goal

3D-regrid from the global geographical input to the regional LCC output (as conservatively as possible). Unfortunately this particular 3D-regridding is not currently (early 2013) well-supported by existing tools; particularly

* GRASS, [NCL][NCL @ wikipedia], and [R][R @ wikipedia] interpolations do not support 3D directly
* ESMF regridding tools do not support spherical->Cartesian directly
* no tools support vertical [HσP][hybrid sigma-pressure @ wikipedia] directly

[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29

## plan

### constraints

Plan and implementation constraints include:

#### nodal grids vs center grids

I have found only one tool that currently supports 3D regridding in any form, [ESMF_RegridWeightGen][ESMF_RegridWeightGen node in ESMF refdoc]. The ESMF regridding tools consider [LCC][LCC @ wikipedia] to be a Cartesian grid, so I will need to convert my input to Cartesian form (i.e., `(x,y,z)` where the dimensions are all distances, not angles). But my input and output grids are also *not grids in the same sense as ESMF* uses. The input and output grids attribute values (concentrations) to the spaces interior to sets of grid nodes: implicitly, they attribute values to centers of hexahedronal gridcells. But the ESMF regridding tools operate on grids of nodes, i.e., the vertices or corners of hexahedrons. So, from both the input and output, I need to extract the grid of nodes.

#### two-pass regridding

The ESMF regridding tools work by

1. generating weights that map (size of) the input grid to the output grid
2. applying those weights to the values of the input grid

... but, currently, only for spherical grids. For Cartesian grids, weight application is not currently supported, so I will need to generate the weights, then apply them "manually."

#### need for surface elevation data

### details

Use [ESMF][ESMF_RegridWeightGen node in ESMF refdoc], [NCL][NCL @ wikipedia], and [R][R @ wikipedia] (driven by [bash][] scripts) to

1. Find surface elevation data. (Went with [ETOPO1][ETOPO1 surface elevation data].)
1. "Cartesianize" the input grid.
    1. (R) Plot global, spherical/lon-lat [MOZART][]-outputted N2O concentrations for eyeballing against future assimilations.
    1. (R) Crop *raw input* (global, spherical/lon-lat data on N2O concentrations, surface pressure (PS), and surface elevation (zS)) to the horizontal extents of the (output) [AQMEII-NA domain][EPA implementation of AQMEII-NA] (and plot for verification). Note that
        * only data variables={`N2O`, `PS`, `zS`} have horizontal coordinates
        * data variable=`PS` has *only* horizontal coordinates (not vertical)
        * [HσP][hybrid sigma-pressure @ wikipedia] data variables={`hyai`, `hyam`, `hybi`, `hybm`} have only vertical coordinates (and no units)
        * data variable=`P0` (reference pressure) has neither coordinates nor units
    1. (R) Regrid cropped surface elevation grid to match cropped {`N2O`, `PS`} grids.
    1. (NCL) Compute layer-interface elevations, producing new datavar=`double z_int_geo(ilev, lat, lon)` in [`2008N2O_restart_region_z.nc`][2008N2O_restart_region_z.nc] using [`NCL::pres_hybrid_ccm`][pres_hybrid_ccm] and [`NCL::stdatmus_p2tdz`][stdatmus_p2tdz]. Replaced missing values output by `NCL::stdatmus_p2tdz` with geographically-corresponding values from ETOPO1:

        * if ETOPO1 value >= 0: replace missing value with ETOPO1 value, and add latter to all above-surface layer interfaces
        * if ETOPO1 value < 0 (mostly oceanic): replace missing value with 0

    1. (NCL) Compute layer-midpoint elevations, producing new datavar=`double z_mid_geo(lev, lat, lon)` in [`2008N2O_restart_region_z.nc`][2008N2O_restart_region_z.nc] by taking the mean of the adjacent layer interfaces (and checking that they are vertically monotonic).

    1. (NCL, R) Create purely Cartesian (x,y,z) datavars for consumption by [`ESMF_RegridWeightGen`][ESMF_RegridWeightGen node in ESMF refdoc]. Caveats:
        1. Datavar for input (i.e., regrid from) completed (in this step=2), datavar for output (i.e., regrid to) in process (following step=3). Latter will require some code refactoring (or just plain dumb copy/mod).

1. *(in process)* Cartesianize the output grid.
    1. (NCL) Copy one datavar (sufficient to define dimensions) from original output to *pruned output*.
    1. (R) Convert horizontal grid nodes from LCC `([hσp][hybrid sigma-pressure @ wikipedia],col,row)` in pruned output to geographical `(hσp,lat,lon)` in *geographicalized output*.
    1. *(planned)* (NCL) Convert vertical layer-interface (i.e., grid node) values from [HσP][hybrid sigma-pressure @ wikipedia] to elevations using [`NCL::pres_hybrid_ccm`][pres_hybrid_ccm] and [`NCL::stdatmus_p2tdz`][stdatmus_p2tdz], replaced missing values output by `NCL::stdatmus_p2tdz` with geographically-corresponding values from ETOPO1:

        * if ETOPO1 value >= 0: replace missing value with ETOPO1 value, and add latter to all above-surface layer interfaces
        * if ETOPO1 value < 0 (mostly oceanic): replace missing value with 0

    1. *(planned)* Compute layer-midpoint elevations by taking the mean of the adjacent layer-interface elevations (and checking that they are vertically monotonic).
    1. *(planned)* (NCL, R) Create purely Cartesian (x,y,z) datavars for consumption by [`ESMF_RegridWeightGen`][ESMF_RegridWeightGen node in ESMF refdoc].
1. *(planned)* Use [`ESMF_RegridWeightGen`][ESMF_RegridWeightGen node in ESMF refdoc] to generate regrid weights from the Cartesianized input and output grids. Caveats:
        1. There is currently no code for generating the `nodeCoords` and `elementConn` datavars for the *[ESMF Unstructured Grid File Format][ESMF Unstructured Grid File Format node in ESMF refdoc]*. However I do have (on unfortunately private list `esmf_support@list.woc.noaa.gov`) pseudocode from [Bob Oehmke][] for this.
1. *(planned)* Use regrid weights to calculate and attribute values (concentrations) to output gridcells.

[MOZART]: http://en.wikipedia.org/wiki/MOZART_(model)
[EPA AQMEII-NA PROJ.4]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain#!proj4
[hybrid sigma-pressure @ wikipedia]: http://en.wikipedia.org/wiki/Sigma_coordinate_system#hybrid_sigma-pressure
[ETOPO1 surface elevation data]: http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface/grid_registered/netcdf/
[bash]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[stdatmus_p2tdz]: http://www.ncl.ucar.edu/Document/Functions/Built-in/stdatmus_p2tdz.shtml
[pres_hybrid_ccm]: http://www.ncl.ucar.edu/Document/Functions/Built-in/pres_hybrid_ccm.shtml
[2008N2O_restart_region_z.nc]: ../../downloads/2008N2O_restart_region_z.nc
[PHASE 2008 homepage]: http://www.cdc.gov/nceh/tracking/phase.htm
[ESMF_RegridWeightGen node in ESMF refdoc]: http://www.earthsystemmodeling.org/esmf_releases/non_public/ESMF_6_1_0/ESMF_refdoc/node3.html#SECTION03020000000000000000
[ESMF Unstructured Grid File Format node in ESMF refdoc]: http://earthsystemmodeling.org/esmf_releases/non_public/ESMF_6_1_1/ESMF_refdoc/node3.html#SECTION03025000000000000000
[Bob Oehmke]: http://www.esrl.noaa.gov/nesii/about/staff/bob.html

# implementation

To run the examples:

1. Clone this repo.
2. `cd` to its working directory.
3. Process MOZART-side data:
    1. Visualize the global data:
        1. Edit ./[levelplot_netCDF_global.sh][] to match your local configuration (e.g., set your PDF viewer).
        2. Run `./levelplot_netCDF_global.sh` to visualize the raw data.
        3. Check your resulting visualization against [the reference (download)][levelplot_netCDF_global.pdf].
    1. Create and visualize regional subsets of the global data:
        1. Edit ./[restrict_lon_lat_to_region__MOZART.sh][] to match your local configuration.
        2. Run `./restrict_lon_lat_to_region__MOZART.sh` to:
            1. crop the global raw input data to the region (and save separately).
            2. visualize the regional data.
        3. Check your resulting visualizations against provided references (downloads):
            * [`2008N2O_restart_region_N2O.pdf`][2008N2O_restart_region_N2O.pdf]
            * [`2008N2O_restart_region_PS.pdf`][2008N2O_restart_region_PS.pdf]
            * [`ETOPO1_Ice_g_gmt4.grd_region_zS.pdf`][ETOPO1_Ice_g_gmt4.grd_region_zS.pdf]
    1. Regrid the surface elevation data to match the grid of the other regional inputs:
        1. Edit ./[regrid_surface_elevation__MOZART.sh][] to match your local configuration.
        2. Run `./regrid_surface_elevation__MOZART.sh` to:
            1. regrid the surface elevation data (and save separately).
            2. visualize the regridded data.
        3. Check your resulting visualizations against [the reference (download)][ETOPO1_Ice_g_gmt4.grd_region_zS_regrid.pdf].
    1. Compute layer-interface and layer-midpoint elevations:
        1. Edit ./[hsp_to_gh_amsl__MOZART.sh][] to match your local configuration.
        1. Run `./hsp_to_gh_amsl__MOZART.sh`.
        1. Check the values of the resulting new elevation datavars (e.g.,

                double z_int_geo(ilev, lat, lon) # layer-interface elevations on geographical coordinates
                double z_mid_geo(lev, lat, lon)  # layer-midpoint elevations on geographical coordinates

         ) in the resulting netCDF output against [the reference (download)][2008N2O_restart_region_z.nc]. (Yes, this needs visualizations ...)
        1. Check the values of `z_int_geo` written (as ASCII CSV, for data interchange with R) to [the reference (download)][2008N2O_restart_region_z.txt]. (Yes, this also needs visualizations ...)

    1. Compute fully-Cartesian layer-interface elevations:
        1. Edit ./[cartesianize_elevations.sh][] to match your local configuration.
        1. Run `./cartesianize_elevations.sh`. Tested on both tlrPanP5 and terrae.
        1. TODO: provide visualization to check the values of the resulting elevation datavar==`nodeCoords`

4. Process CMAQ-side data:

[levelplot_netCDF_global.sh]: ../../src/HEAD/levelplot_netCDF_global.sh?at=master
[levelplot_netCDF_global.pdf]: ../../downloads/levelplot_netCDF_global.pdf
[restrict_lon_lat_to_region__MOZART.sh]: ../../src/HEAD/restrict_lon_lat_to_region__MOZART.sh?at=master
[2008N2O_restart_region_N2O.pdf]: ../../downloads/2008N2O_restart_region_N2O.pdf
[2008N2O_restart_region_PS.pdf]: ../../downloads/2008N2O_restart_region_PS.pdf
[ETOPO1_Ice_g_gmt4.grd_region_zS.pdf]: ../../downloads/ETOPO1_Ice_g_gmt4.grd_region_zS.pdf
[ETOPO1 surface elevation data]: http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface/grid_registered/netcdf/
[ETOPO1_Ice_g_gmt4.grd_region_zS_regrid.pdf]: ../../downloads/ETOPO1_Ice_g_gmt4.grd_region_zS_regrid.pdf
[2008N2O_restart_region_z.nc]: ../../downloads/2008N2O_restart_region_z.nc
[2008N2O_restart_region_z.txt]: ../../downloads/2008N2O_restart_region_z.txt
[hsp_to_gh_amsl__MOZART.sh]: ../../src/HEAD/hsp_to_gh_amsl__MOZART.sh?at=master
[regrid_surface_elevation__MOZART.sh]: ../../src/HEAD/regrid_surface_elevation__MOZART.sh?at=master
[cartesianize_elevations.sh]: ../../src/HEAD/cartesianize_elevations.sh?at=master

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `netCDF.stats.to.stdout.r`
1. Move all these TODOs to [issue tracker][MOZART_global_to_AQMEII-NA issues].
1. (next major item) Plan item=3.3: read CSV back to NCL, convert `(hσp,lat,lon)` to `(x,y,z)`.
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. Debug why writing CSV from NCL is so slow (at least, as I'm currently doing it, on `terrae`): netCDF datavar (probably most of 13 MB file) takes 19.22 min to write a 66 MB CSV.
1. Debug why [`hsp_to_gh_amsl__MOZART.ncl`][hsp_to_gh_amsl__MOZART.ncl] converts `type(z_int)` float->double.
1. Debug why [`regrid_surface_elevation__MOZART.r`][regrid_surface_elevation__MOZART.r] `input.max(z) > output.max(z)`, when just regridding over same region.
1. Ping `r-sig-geo` regarding raster datatype truncation.
1. Refactor the now-6 sets of {.sh, .ncl, .r}.
1. Investigate fringe of apparently-missing data along north and east borders of surface-elevation plot.
1. Create common project for `regrid_resources` à la [`regrid_utils`][regrid_utils], so I don't hafta hunt down which resource is in which project.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[MOZART_global_to_AQMEII-NA issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[hsp_to_gh_amsl__MOZART.ncl]: ../../src/HEAD/hsp_to_gh_amsl__MOZART.ncl?at=master
[regrid_surface_elevation__MOZART.r]: ../../src/HEAD/regrid_surface_elevation__MOZART.r?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master

<!--
Above see markdown for nesting code, and continuation of a (parent) list item after a nested sublist.
Note that BB doesn't render markdown as well as Markdown.pl!
E.g., this comment is visible in BB, but not in Markdown.pl-generated HTML :-(
-->
